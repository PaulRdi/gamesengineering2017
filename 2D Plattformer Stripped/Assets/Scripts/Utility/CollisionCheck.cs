﻿using UnityEngine;
using System.Collections;

public delegate void TriggerDelegate(Collider2D other);

[RequireComponent(typeof(Collider2D))]
public class CollisionCheck : MonoBehaviour {

    public event TriggerDelegate TriggerEntered;

	void OnTriggerEnter2D(Collider2D other)
    {

        if (TriggerEntered != null)
            TriggerEntered(other);

    }

    void Start()
    {
        if (!this.GetComponent<Collider2D>().isTrigger)
            Debug.LogWarning("Der Collider 2D ist nicht als Trigger gesetzt!");
    }
}
