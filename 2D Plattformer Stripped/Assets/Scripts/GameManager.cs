﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {


    // Schwarze Magie, nicht anfassen.
    #region Singleton
    static GameManager instance_;
    public static GameManager instance
    {
        get
        {
            if (instance_ == null)
                FindGameManager();

            return instance_;
        }
    }

    void Awake()
    {
        if (instance_ == null)
            FindGameManager();
        else
            Destroy(this.gameObject);
    }

    static void FindGameManager()
    {
        GameObject inst = GameObject.Find("GameManager");
        instance_ = inst.GetComponent<GameManager>();
    }
    #endregion

    /*
     * Hier ist Platz für Variablen, die keinem konkreten Objekt zugeordnet sind.
     * 
     * Eine Variable hier ist jeder Zeit über GameManager.instance.VARIABLEN_NAME aufrufbar
     * 
     * Ein Beispiel wäre: GameManager.instance.score = GameManager.instance.score + 1;
     */

    public int score = 0;
}
