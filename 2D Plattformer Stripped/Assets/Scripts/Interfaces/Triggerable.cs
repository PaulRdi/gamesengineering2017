﻿using UnityEngine;
using System.Collections;

/* Nicht anfassen */
[RequireComponent(typeof(CollisionCheck))]
public abstract class Triggerable : MonoBehaviour
{

    protected CollisionCheck the_collider;
    protected GameObject the_player;
    protected Rigidbody2D player_rigidbody;
    protected Transform player_transform;
    protected Vector2 player_position
    {
        set { the_player.transform.position = value; }
        get { return the_player.transform.position; }
    }
    protected Vector2 destination_position
    {
        set {
            if (destination_transform != null)
                destination_transform.position = value;
            else
                Debug.LogError("Du musst im Inspektor ein Ziel-Objekt festlegen!");
        }

        get
        {
            if (destination_transform != null)
                return destination_transform.position;
            else
            {
                Debug.LogError("Du musst im Inspektor ein Ziel-Objekt festlegen!");
                return Vector2.zero;
            }
        }
    }

    [SerializeField][Tooltip("Wenn Zielkoordinaten für etwas vorhanden sein soll (z.B. für einen Teleporter).")]
    protected Transform destination_transform;

    void Start()
    {
        the_player = GameObject.FindGameObjectWithTag("Player");
        the_collider = GetComponent<CollisionCheck>();

        if (the_player != null)
        {
            player_rigidbody = GetComponentWithNullCheck<Rigidbody2D>(the_player);
            player_transform = GetComponentWithNullCheck<Transform>(the_player);
            player_position = the_player.transform.position;
        }
        else
        {
            Debug.LogWarning("Kann kein Objekt mit dem Tag 'Player' finden.");
        }

        if (destination_transform != null)
            destination_position = destination_transform.position;

        the_collider.TriggerEntered += this.The_collider_TriggerEntered;
    }

    void OnDestroy()
    {
        the_collider.TriggerEntered -= this.The_collider_TriggerEntered;
    }

    private void The_collider_TriggerEntered(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            FuehreAktionBeiTriggerKollisionAus();
    }


    public abstract void FuehreAktionBeiTriggerKollisionAus();

    T GetComponentWithNullCheck<T>(GameObject obj)
    {
        T v;
        if (obj.GetComponent<T>() != null)
        {
            v = obj.GetComponent<T>();
        }
        else
        {
            Debug.LogWarning("You tried to get a component which isn't on the Object");
            v = default(T);
        }
        return v;
    }
}
