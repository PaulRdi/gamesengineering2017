﻿using UnityEngine;
using System.Collections;
using System;

public class TemplateBehaviour : Triggerable {

    public override void FuehreAktionBeiTriggerKollisionAus()
    {
        /*
         * Alles was hier drin steht passiert, sobald der Spieler den Trigger, des Objekts
         * auf dem dieses Script liegt, betritt.
         * 
         * Es ist hierfür notwendig, dass das Objekt auf dem dieses Script liegt ein
         * CollisionCheck Script hat. (Zu finden unter Scripts/Utility)
         * 
         * Damit das Collision Check Script funktioniert, ist wiederum eine Collider2D
         * (z.B. ein BoxCollider2D) Komponente nötig, die als Trigger markiert ist
         * (einfach das Häkchen 'isTrigger' auf aktiv setzen)
         * 
         * Es gibt die Variablen: player_position, destination_position, player_rigidbody
         */
    }

}
